﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;


namespace OtusPLinq
{
    internal class Program
    {
       const int Count = 1_000_000;
        const int ProcessCount = 4;
        static int[] array = new int[Count];
        static Random random = new Random();
        static void Main(string[] args)
        {
            initiateArray();
            TimeTest(() => SumArray());
            TimeTest(() => SumArrayTaskToProcesses());
            TimeTest(() => SumArrayTaskToProcessesAsParallel());
            Console.ReadKey();
        }
        static void initiateArray()
        {
            for (int i = 0; i < Count; i++)
            {
                array[i] = random.Next(0, 1000);
            }
        }
        static int SumArray()
        {
            Console.WriteLine("Расчет без распараллеливания");
            int sum = 0;
            for (int i = 0; i < Count; i++)
            {
                sum += array[i];
            }
            return sum;
        }
        static int SumArrayTaskToProcesses()
        {
            Console.WriteLine("Расчет с объектами Task вызванных на ядрах процессора.");
            int ProcessCount = Environment.ProcessorCount;
            int range = Count / ProcessCount;
            List<Thread> threads = new List<Thread>();
            int[] sumArray = new int[ProcessCount];
            for (int p1 = 0; p1 < ProcessCount; p1++)
            {
                int p = p1;
                int start = p * range;
                int end = (p == ProcessCount - 1) ? Count : start + range;
                Thread thread = new Thread(() =>
                {
                    for (int i = start; i < end; i++)
                    {
                        sumArray[p] += array[i];
                    }
                }
                );
                threads.Add(thread);
                thread.Start();
            }

            
            return sumArray.Sum();
        }
        static int SumArrayTaskToProcessesAsParallel()
        {
            Console.WriteLine("Расчет с LINQ AsParallel");
            return array.AsParallel().Sum();
        }
        static void TimeTest(Func<int> func)
        {
            Stopwatch stopwath = new Stopwatch();
            stopwath.Start();
            Console.WriteLine($"Сумма элементов массива {func.Invoke()}");
            stopwath.Stop();
            TimeSpan ts = stopwath.Elapsed;
            Console.WriteLine($"Затрачено {ts.Milliseconds} милисекунд");
        }

    }
}